Feature: Login test validation

  Scenario Outline: Validate login successful scenarios

    Given I navigate to DirectScale login page
    And I enter username as "<username>"
    And I enter password as "<password>"
    When I click on login button
    And I navigate to profile page
    Then I validate the display name is "<displayname>"
    And I close the browser

    Examples: Login credentials

    | username | password        | displayname  |
    | gandalf  | Sh@d0wf@x       | white wizard |

  Scenario Outline: Validate login unsuccessful scenarios

    Given I navigate to DirectScale login page
    And I enter username as "<username>"
    And I enter password as "<password>"
    When I click on login button
    Then I validate I cannot log in
    And I close the browser

    Examples: Login credentials

      | username | password        |
      | saruman  | youshallnotpass |
