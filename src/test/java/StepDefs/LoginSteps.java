package StepDefs;

import cucumber.api.java.en.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class LoginSteps {

    private static Properties CONFIG = new Properties();
    private static WebDriver driver;
    private static WebDriverWait wait;

    @Given("^I enter username as \"([^\"]*)\"$")
    public void i_enter_username_as(String username) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CONFIG.getProperty("username"))));
        driver.findElement(By.xpath(CONFIG.getProperty("username"))).sendKeys(username);
    }

    @Given("^I enter password as \"([^\"]*)\"$")
    public void i_enter_password_as(String password) {
        driver.findElement(By.xpath(CONFIG.getProperty("password"))).sendKeys(password);
    }

    @When("^I click on login button$")
    public void i_click_on_login_button() {
        driver.findElement(By.xpath(CONFIG.getProperty("submitBtn"))).click();
    }

    @When("^I navigate to profile page$")
    public void i_navigate_to_profile_page() throws IOException {
        CONFIG.load(new FileInputStream(
                System.getProperty("user.dir") + "\\src\\test\\java\\PageObjects\\WelcomPage.properties"));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CONFIG.getProperty("optionsDropdown"))));
        String profileURL = driver.getCurrentUrl().
                substring(0, driver.getCurrentUrl().lastIndexOf("Corporate") + "Corporate".length()) + CONFIG.getProperty("profileURL");
        driver.navigate().to(profileURL);
    }

    @Then("^I validate the display name is \"([^\"]*)\"$")
    public void i_validate_I_successfully_logged_in(String displayname) throws IOException {
        CONFIG.load(new FileInputStream(
                System.getProperty("user.dir") + "\\src\\test\\java\\PageObjects\\ProfilePage.properties"));
        String actualText = driver.findElement(By.xpath(CONFIG.getProperty("displayname"))).getText();
        Assert.assertEquals(actualText, displayname, "Actual display name does not match with expected display name");
    }

    @Then("^I validate I cannot log in$")
    public void i_validate_I_cannot_log_in() {
        driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS) ;
        String failureMessage = driver.findElement(By.xpath(CONFIG.getProperty("failureMsg"))).getText();
        Assert.assertEquals(failureMessage, "Authentication Failed", "Failure message doesn't match");
    }

    @Then("^I close the browser$")
    public void i_close_the_browser() {
        driver.close();
    }

    private void loadProperties() throws IOException {
        CONFIG.load(new FileInputStream(
                System.getProperty("user.dir") + "\\src\\test\\java\\PageObjects\\Login.properties"));
        System.setProperty("webdriver.chrome.driver",
                "F:\\Sid\\Eclipse\\Selenium WebDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 20);
    }

    @Given("^I navigate to DirectScale login page$")
    public void iNavigateToDirectScaleLoginPage() throws IOException {
        loadProperties();
        driver.get(CONFIG.getProperty("URL"));
        driver.manage().window().maximize() ;
    }
}
