To run the individual test file or feature file, please install following plugins in your IDE-
    1. Gherkin plugin for Eclipse/IntelliJ Idea
    2. Cucumber plugin for Eclipse/IntelliJ Idea

You can clone this project and compile maven project using command - "mvn clean install"
You can run the project by right clicking file and run individual feature file.